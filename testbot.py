import asyncio
# import redis
import random
import ruamel.yaml
from string import Formatter

from sanic import Sanic
from sanic.response import html
from sanic import Blueprint, response
from sanic.request import Request
from sanic.response import HTTPResponse
from typing import Optional, Text, Any, List, Dict, Iterable, Callable, Awaitable
from datetime import date

import socketio
import logging
import re
import requests
import copy
import json


logging.basicConfig(level = logging.INFO)
logger = logging.getLogger(__name__)


app = Sanic()

states = {}

@app.route('/webhook', methods=["GET","POST"])
async def webhook(request):
    if request.method == "GET":
        return response.json({"status": "ok"})

    data = request.json
    logger.info(data)
    sender = data['sender']
    message = data['message']

    if message == "terug naar gem":
        responsedata = [
            {"text": "ik verbind je weer met gem"}, {"custom": {"deescalate": True}}
        ]
        return response.json(responsedata)

    #process nlu
    url = "https://www.develop.virtuele-gemeente-assistent.nl/model/parse"
    nludata = {"text":message}
    nluresp = requests.post(url, json = nludata).json()
    logger.info(nluresp)
    intent = nluresp['intent']
    intentname = intent['name']
    confidence = intent['confidence']
    entities = nluresp.get('entities')
    responsetext = 'Lorum Ipsum'
    questions = [
        'Wat is je geboortedatum?',
        'Wat is je email adres?',
        'Wat is je adres?',
        'Wat is je telefoonnummer?',
        'Ben je vandaag blij?',
        'Wanneer ben je jarig?',
        'Wat is je achternaam?',
        'Wat is je postcode?',
        'Wat is je woonplaats?',
        'Hoeveel broers of zussen heb je?'
    ]
    if confidence > 0.6:
        if intentname == 'greet':
            responsetext = 'Hoi ik ben een test bot, ik stel je wat vragen, probeer je me antwoorde te geven?'
        if intentname.startswith('inform'):
            cdata = {e.get('entity'):e.get('value') for e in entities}
            responsetext = json.dumps(cdata).replace('"', '').replace(',', '  \n')[1:-1]
    else:
        responsetext = 'Ik weet niet wat je bedoelt, maar misschien stelde ik een open vraag'

    responsedata = [
      {"text": responsetext},{"text": random.choice(questions)}
    ]

    return response.json(responsedata)

@app.route("/", methods=["GET"])
async def health(_: Request) -> HTTPResponse:
    return response.json({"status": "ok"})

if __name__ == '__main__':
    app.run(host="0.0.0.0")