# base image
FROM python:3

# set working directory
WORKDIR /usr/src/myapp

# copy the dependencies file to the working directory
COPY ./requirements.txt /usr/src/myapp/requirements.txt

# install dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Copy testbot.py to the working directory
COPY ./testbot.py /usr/src/myapp/testbot.py

